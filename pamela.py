import syslog
import subprocess

###
#       Configuration
#       TO DO: A configuration file manager
###
g_users = {'toto': 'user_1', 'titi': 'user_2'}
g_users_dir = "/home/users/"

g_pamela = None

###
#       PAMELA
#       This class is used to manage a Luks volume such as mounting/unmounting
###
class   pamela:
    HOME_DIR = "/home/"

    def __init__(self, src, mapping):
        self.src = src
        self.mapping = mapping

    def __invoke_cmd(self, cmd):
        return subprocess.call(cmd, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)

    def __mount(self):
        return self.__invoke_cmd("mount /dev/mapper/%s %s" % (self.mapping, self.HOME_DIR))

    def __umount(self): 
        return self.__invoke_cmd("umount %s" % (self.HOME_DIR))

    def __luks_open(self, passwd):
        return self.__invoke_cmd("echo '%s' | cryptsetup luksOpen %s %s" % (passwd, self.src, self.mapping))

    def __luks_close(self):
        return self.__invoke_cmd("cryptsetup luksClose %s" % (self.mapping))

    def start(self, passwd):
        if self.__luks_open(passwd) != 0 or self.__mount() != 0:
            syslog.syslog(syslog.LOG_AUTH, "Could not open luks volume or mount user container.")
            return False
        return True

    def end(self):
        if self.__umount() != 0 or self.__luks_close() != 0:
            syslog.syslog(syslog.LOG_AUTH, "Could not close luks volume or unmount user container.")
            return False
        return True

###
#       GET AUTHENTIFICATE
#       Because there's no config file management, this has been done.
###
def get_auth(pamh, passwd):
    global g_pamela

    for user in g_users:
        if user == pamh.user:
            g_pamela = pamela("/home/users/" + g_users[user], g_users[user]);
            pamh.conversation(pamh.Message(pamh.PAM_TEXT_INFO, "Opening and mounting luks volume... please wait..."))
            return g_pamela.start(passwd)
    return False

###
#       PAM MODULES
#       These functions are called by the PAM framework.
###
def pam_sm_authenticate(pamh, flags, argv):
    pamh.conversation(pamh.Message(pamh.PAM_TEXT_INFO, "PAMELA"))
    syslog.syslog(syslog.LOG_AUTH, "Trying to get authentificated.")
    if pamh.user == "root":
        return pamh.PAM_SUCCESS
    luks_passwd = pamh.conversation(pamh.Message(pamh.PAM_PROMPT_ECHO_OFF, "Luks volume password: ")).resp
    if luks_passwd == None:
        pamh.conversation(pamh.Message(pamh.PAM_ERROR_MSG, "Bad password!"))
        syslog.syslog(syslog.LOG_AUTH, "Authentification failure: no password.")
        return pamh.PAM_AUTH_ERR
    if get_auth(pamh, luks_passwd) == False:
        pamh.conversation(pamh.Message(pamh.PAM_TEXT_INFO, "Authentification failed."))
        syslog.syslog(syslog.LOG_AUTH, "Authentification failure: get_auth() failed.")
        return pamh.PAM_AUTH_ERR
    syslog.syslog(syslog.LOG_AUTH, "Authentification success.")
    return pamh.PAM_SUCCESS

def pam_sm_end(pamh):
    syslog.syslog(syslog.LOG_AUTH, "Closing all pamela ressources...")
    if g_pamela.end() == False:
        syslog.syslog(syslog.LOG_AUTH, "Failed at closing ressources. Pamela won't work if you don't fix it.")
        return pamh.PAMH_AUTH_ERR
    return pamh.PAM_SUCCESS

def pam_sm_setcred(pamh, flags, argv):
    return pamh.PAM_SUCCESS

def pam_sm_acct_mgmt(pamh, flags, argv):
    return pamh.PAM_SUCCESS

def pam_sm_open_session(pamh, flags, argv):
    return pamh.PAM_SUCCESS

def pam_sm_close_session(pamh, flags, argv):
    return pamh.PAM_SUCCESS

def pam_sm_chauthtok(pamh, flags, argv):
    return pamh.PAM_SUCCESS
