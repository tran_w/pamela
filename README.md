This was tested on a Debian x64 operating system.

# Requirements
* Python 2.7
* libpam-python
* python-pam
* cryptsetup

# Description
This is a pam module written in Python. It allows you to authentificate on an luks volume.

# Usage
* Put the file 'pamela.py' in /lib/security. 
* Add at the end of the file, the following line /etc/pam.d/login:  
auth sufficient pam_python.so /lib/security/pamela.py
* Containers must be in /home/users/ (watch the code for more informations)

# What is LUKS?
In computing, the Linux Unnified Key Setup or LUKS is a disk-encryption sepcification created by Clemens Fruhwirth and orignally intended for Linux.  
source: wikipedia.org

# Why should I use an encryption system such as LUKS?
Today, our data are not safe of being robben. If someone steal you computer and you think your data are safe because you've put a password on it; you're totally wrong!  
Indeed, the thief may use a liveCD to mount your partition and access to what he desires. That's why, you should use an encryption system such as LUKS. If he stole your computer he can barely use the data on it.